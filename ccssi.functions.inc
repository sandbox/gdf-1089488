<?php
// $Id$

/**
 * @file
 * functions used by the ccssi module.
 *
 * most of the functionality of the module is located here.
 */


/******************************************************************************/

//
// CCSSI auxiliary data functions
//
// return some auxiliary table value, e.g. cluster_text for cluster_id=12
// - or -
// return auxiliary table as array indexed by xxxx_id
//

/**
 * (private) retrieve data from auxiliary table
 * 
 * @param $datatype
 *   name of auxiliary table
 * @param $id_field
 *   name of primary key field
 * @param $id
 *   (optional) ID of row to be retrieve.  If null, return whole table
 * @param $criteria
 *   (optional) additional WHERE clause for query
 * 
 * @return
 *   if $id is not NULL, return single row: else, return array of table elements
 */

function _ccssi_aux_data( $datatype, $id_field, $id = NULL, $criteria = NULL ) {
  // dpm($id);
  $cache_name = "ccssi_aux_data_{$datatype}";
  if ( $criteria != NULL ) {
    $cache_name .= '_' . preg_replace( '/[^a-z0-9_]/i', '_', $criteria );
    $criteria = "WHERE $criteria";
  }
  if ( $datatype == 'grade_level' ) {
    $criteria .= " ORDER BY grade_sort";
  }
  // dpm("cache_name: {$cache_name}");
  if ( isset($_SESSION[$cache_name]) ) {
  // if ( 0 ) {
  $ccssi_aux_data = $_SESSION[$cache_name];
  }
  else {
    // don't have data in SESSION; will need to retrieve it
    $cache = cache_get($cache_name, 'cache');
    if ( empty($cache) ) {
      // not in cache; query for it
      // dpm('query');
      $result = db_query( "SELECT * from {ccssi_{$datatype}} $criteria" );
      $ccssi_aux_data = array();
      while ($row = db_fetch_array($result) ) {
        $ccssi_aux_data[$row[$id_field]] = $row;
        // dpm($row);
      }
      cache_set( $cache_name, $ccssi_aux_data, 'cache', CACHE_PERMANENT );
    }
    else {
      $ccssi_aux_data = $cache->data;
    }
    $_SESSION["ccssi_aux_data_{$datatype}"] = $ccssi_aux_data;
  }
  if ($id) return $ccssi_aux_data[$id];
  else return $ccssi_aux_data;
}

/******************************************************************************/

/**
 * query CCSSI database for standards that meet certain criteria
 * 
 * @param $query_params
 *   Array of target=>value elements, where:
 *   - *target* is some documented query parameter, and
 *   - *value* is either a single value or an array of values
 * @param $result_options
 *   (optional) Array of option=>value elements, where:
 *   - *option* is a keyword that modifies format of the result array, and
 *   - *value* is its optional value
 *
 * Example usage:
 * @code
 *   $result = ccssi_query( array( 'grade_id'=>'4', 'deficit_area'=>array(1, 4)' ) );
 * @endcode
 */
function _ccssi_query( $query_params, $result_options = NULL ) {

  //
  // process query parameters
  //
  
  foreach ( $query_params as $key => $value ) {
    if ( $key=='grade_level' ) {
      $JOINS['grade_level'] = 'JOIN ccssi_ls_x_gl lsxgl on ls.standard_id=lsxgl.standard_id';
      $WHERES['grade_level'] = "lsxgl.grade_id " . _ccssi_alpha_query_target($value);
    }
    elseif ( $key=='deficit_area' ) {
      $JOINS['deficit_area'] = 'JOIN ccssi_ls_x_da lsxda ON ls.standard_id=lsxda.standard_id';
      $WHERES['deficit_area'] = "lsxda.deficit_id " . _ccssi_alpha_query_target($value);
    }
    elseif ( $key=='subject_area' ) {
      $WHERES['subject_area'] = "ls.subject_id " . _ccssi_numeric_query_target($value);
    }
    elseif ( $key=='superdomain' ) {
      $WHERES['superdomain'] = "ls.superdomain_id " . _ccssi_alpha_query_target($value);
    }
    elseif ( $key=='domain' ) {
      $WHERES['domain'] = "ls.domain_id " . _ccssi_alpha_query_target($value);
    }
    elseif ( $key=='cluster' ) {
      $WHERES['cluster'] = "ls.cluster_id " . _ccssi_numeric_query_target($value);
    }
    else {
      drupal_set_message("ccssi_query: unknown query parameter {$key}=>{$value}", "error");
    }
  }
  
  //
  // process result options
  //
  
  foreach ( $result_options as $option => $value ) {
    if ( $option=='RETURNTEXT') {
      $JOINS['grade_level'] = 'LEFT OUTER JOIN ccssi_ls_x_gl lsxgl on ls.standard_id=lsxgl.standard_id';
      $JOINS['grade_text'] = 'LEFT OUTER JOIN ccssi_grade_level gl on lsxgl.grade_id=gl.grade_id';
      $SELECTS['grade_text'] = 'gl.grade_name';
      $JOINS['superdomain'] = 'LEFT OUTER JOIN ccssi_superdomain sdom on ls.superdomain_id=sdom.superdomain_id';
      $SELECTS['superdomain'] = 'sdom.superdomain_text';
      $JOINS['domain'] = 'LEFT OUTER JOIN ccssi_domain dom on ls.domain_id=dom.domain_id';
      $SELECTS['domain'] = 'dom.domain_text';
      $JOINS['cluster'] = 'LEFT OUTER JOIN ccssi_cluster cl on ls.cluster_id=cl.cluster_id';
      $SELECTS['cluster'] = 'cl.cluster_text';
      $JOINS['subject'] = 'LEFT OUTER JOIN ccssi_subject sb on ls.subject_id=sb.subject_id';
      $SELECTS['subject'] = 'sb.subject_name';
    }
    elseif ( $option=='RETURNSTRUCTURE' ) {
      $RETURNSTRUCTURE = TRUE;
    }
    elseif ( $option=='RETURNGRADELEVELS') {
      $RETURNGRADELEVELS = TRUE;
      $JOINS['grade_level'] = 'LEFT OUTER JOIN ccssi_ls_x_gl lsxgl on ls.standard_id=lsxgl.standard_id';
      $SELECTS['grade_level'] = 'lsxgl.grade_id';
    }
    elseif ( $option=='RETURNDEFICITAREAS') {
      $RETURNDEFICITAREAS = TRUE;
      $JOINS['deficit_area'] = 'LEFT OUTER JOIN ccssi_ls_x_da lsxda on ls.standard_id=lsxda.standard_id';
      $SELECTS['deficit_area'] = 'lsxda.deficit_id';
    }
    else {
      drupal_set_message("ccssi_query: unknown return option {$option}=>{$value}", "error");
    }
  }
  
  //
  // build query
  //
  
  $sql =
  'SELECT
    ls.standard_id, 
    ls.subject_id,
    ls.section,
    ls.superdomain_id, 
    ls.domain_id, 
    ls.cluster_id,
    ls.standard_number,
    ls.standard_letter,
    ls.advanced,
    ls.modeling,
    ls.standard_text';
  
  if ( count($SELECTS) ) $sql .= ",\n\t" . implode(",\n\t", $SELECTS);
  
  $sql .= '
  FROM ccssi_standard ls';
  
  if ( count($JOINS) ) $sql .= "\n" . implode("\n", $JOINS);
  
  if ( count($WHERES) ) $sql .= "\nWHERE (" . implode(') AND (', $WHERES) . ')';
  
  $sql .= "\nORDER BY ls.sort_sequence";
  
  // dpm($sql); // return(array());
  
  //
  // execute query
  //
  
  $result = db_query($sql);
  
  //
  // process result
  //
  
  $return = array();
  while ($row = db_fetch_array($result) ) { // TEMPORARY FOR TESTING
    $standard_id = $row['standard_id'];
    // $dpmshow .= "{$standard_id}: {$row['deficit_id']}\n";
    if ( isset($return[$standard_id]) ) {
      if ($RETURNDEFICITAREAS && isset($row['deficit_id'])) {
        if ( isset($return[$standard_id]['deficit_id']) ) {
          $return[$standard_id]['deficit_id'] .= ":{$row['deficit_id']}";
        }
        else {
          $return[$standard_id]['deficit_id'] = $row['deficit_id'];
        }
      }
      if ($RETURNGRADELEVELS && isset($row['grade_id'])) {
        if ( isset($return[$standard_id]['grade_id']) ) {
          $return[$standard_id]['grade_id'] .= ":{$row['grade_id']}";
        }
        else {
          $return[$standard_id]['grade_id'] = $row['grade_id'];
        }
      }
    }
    else {
      $return[$standard_id] = $row;
    }
    // dpm($row);
  }
  // dpm($dpmshow);
  
  return $return;
}

function _ccssi_alpha_query_target( $target ) {
  // $target = explode(':',$target_string);
  if ( is_array($target) ) {
    return "IN ('" . implode("','", $target) . "')";
  }
  else {
    return "= '$target'";
  }
}

function _ccssi_numeric_query_target( $target ) {
  // $target = explode(':',$target_string);
  if ( is_array($target) ) {
    return "IN (" . implode(",", $target) . ")";
  }
  else {
    return "= $target";
  }
}
