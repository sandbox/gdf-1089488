<?php
// $Id$

/**
 * @file
 * include functions for module and install
 * 
 */

function ccssi_import_xml($type) {
  ini_set('memory_limit', '1000M');
  if (!isset($type)) {
    $type = arg(2);
    // dpm($type);
  }
  $type = check_plain($type);
  
  if ($type == 'math') {
    $xml_file = CCSSI_MATH_XML; 
  }
  elseif ($type == 'ela') {
    $xml_file =  CCSSI_ELA_XML; 
  }
  elseif ($type == 'all') {
    $xml_file =  CCSSI_ALL_XML; 
  }
  else {
    return "url should be of form ccssi/parsexml/ela or ccssi/parsexml/math"; 
  }
  $xml_file = drupal_get_path('module', 'ccssi') . $xml_file;
  // dpm($xml_file);
  $data = _parse_excel_xml_file($xml_file);
  // dpm($data);
  $domains = array();
  $superdomains = array();
  $clusters = array();
  
  $sql = "INSERT INTO {ccssi_standard}
          ( standard_id, subject_id, section,
            superdomain_id, domain_id, cluster_id,
            standard_number, standard_letter, standard_text,
            modeling, advanced, is_supplement, sort_sequence )
          VALUES ('%s', %d, '%s', '%s', '%s', %d, %d, '%s', '%s', %d, %d, %d, %d)";
  
  // $count = 100;
  $sort_sequence = 0;
  foreach ($data as &$row) {
    // if  (($count--)==0) break;
    if ( $row[1] == '' ) continue;
    if ( $row[1] == 'standard_id' ) continue;
    
    $sort_sequence++;
    // dpm($row);
    // build lists of things that are stored in side tables:
    //   superdomain, domain, cluster
    // if item does not have an id column, substitute original text with returned id
    _reference_lister($domains, $row[6], $row[7]);
    _reference_lister($superdomains, $row[4], $row[5]);
    $row[9] = _reference_lister($clusters, $row[9]);
    // adjust subject field
    $row[2] = ($row[2] == 'ELA') ? 1 : 2;
    // build standard-to-grade relation
    $grade = $row[8];
    // grade is either single grade (CCR,K,1-12) or hyphenated list <grade>-<grade>
    $grade = preg_replace('/K/', '0', $grade);
    $grade = preg_replace('/CCR/', '99', $grade);
    // dpm($grade);
    if (preg_match('/(\d+)\-(\d+)/', $grade, $matches)) {
      // dpm('grade range: ' . implode(',',$matches));
    }
    else {
      $matches[1] = $matches[2] = $grade;
    }
    
    if (($matches[1]!=$matches[2]) && ($matches[1]==99 || $matches[2]==99)) {
      drupal_set_message("$row[1]: CCR not a valid grade range value", 'error');
    }
    elseif (!is_numeric($matches[1])) {
      drupal_set_message("$row[1]: grade range start '$matches[1]' not an integer", 'error');
    }
    elseif (!is_numeric($matches[2])) {
      drupal_set_message("$row[1]: grade range end '$matches[2]' not an integer", 'error');
    }
    elseif ( $matches[1]>$matches[2] ) {
      drupal_set_message("$row[1]: grade range out of order?", 'error');
    }
    else {
      for ( $grnum=$matches[1]; $grnum<=$matches[2]; $grnum++ ) {
        if ($grnum==0) $grade_id = 'K';
        elseif ($grnum==99) $grade_id = 'CCR';
        else $grade_id = $grnum;
        $ls_x_gl[] = array( $row[1], $grade_id );
      }
    }
    
    db_query( $sql, $row[1], $row[2], $row[3], $row[5], $row[7], $row[9], $row[10], $row[11], $row[14], $row[13], $row[12], 0, $sort_sequence );
  }
  
  // dpm($data);
  // dpm($domains);
  // dpm($superdomains);
  // dpm($clusters);
  // dpm($ls_x_gl);
  
  $subjects = array(
                1 => 'English Language Arts',
                2 => 'Math',
              );
  $grades = array(
                'CCR' => array( 'College and Career Readiness', 13 ),
                'K' => array( 'Kindergarten', 0 ),
                '1' => array( '1st Grade', 1 ),
                '2' => array( '2nd Grade', 2 ),
                '3' => array( '3rd Grade', 3 ),
                '4' => array( '4th Grade', 4 ),
                '5' => array( '5th Grade', 5 ),
                '6' => array( '6th Grade', 6 ),
                '7' => array( '7th Grade', 7 ),
                '8' => array( '8th Grade', 8 ),
                '9' => array( '9th Grade', 9 ),
                '10' => array( '10th Grade', 10 ),
                '11' => array( '11th Grade', 11 ),
                '12' => array( '12th Grade', 12 ),
              );
  
  $deficit_areas = array(
                1 => array( 'basic reading', 'A'),
                2 => array( 'reading comprehension', 'A'),
                3 => array( 'written expression', 'A'),
                4 => array( 'math calculation', 'A'),
                5 => array( 'math reasoning', 'A'),
                6 => array( 'listening comprehension', 'A'),
                7 => array( 'social / emotional / behavioral', 'SE'),
                8 => array( 'speech / language / communication', 'SE'),
                9 => array( 'daily living / life skills', 'SE'),
                10 => array( 'sensory / motor / physical', 'SE'),
                11 => array( 'learning strategies / skills', 'SE'),
              );
  
  _build_side_table($subjects, 'ccssi_subject', 'subject_id', array('subject_name'), "%d,'%s'");
  _build_side_table($grades, 'ccssi_grade_level', 'grade_id', array('grade_name', 'grade_sort'), "'%s','%s',%d");
  _build_side_table($domains, 'ccssi_domain', 'domain_text', array('domain_id'), "'%s','%s'");
  _build_side_table($superdomains, 'ccssi_superdomain', 'superdomain_text', array('superdomain_id'), "'%s','%s'");
  _build_side_table($clusters, 'ccssi_cluster', 'cluster_text', array('cluster_id'), "'%s',%d");
  _build_side_table($deficit_areas, 'ccssi_deficit_area', 'deficit_id', array('deficit_name', 'deficit_type'), "%d,'%s','%s'");
  _build_relational_table($ls_x_gl, 'ccssi_ls_x_gl', 'standard_id', 'grade_id', "'%s','%s'");
  
  drupal_set_message('CCSSI data imported.', 'status');
  return "output";
}

function _parse_excel_xml_file($xml_file) {

  $xml_string = file_get_contents($xml_file);
  
  $xml_string = preg_replace(
    array(  '/<ss:Data[^>]*\>/',
            '/<\/ss:Data\>/'),
    array(  '<Data><![CDATA[',
            ']]></Data>'),
    $xml_string
  );
  // dpm($xml_string);
  
  $xml=xml2ary($xml_string);
  // dpm($xml);
  
  $rowidx = 0;
  foreach ($xml['Workbook']['_c']['Worksheet'][0]['_c']['Table']['_c']['Row'] as $row) {
    // dpm($row);
    $rowidx++;
    $colidx = 0;
    foreach ($row['_c']['Cell'] as $cell) {
      $colidx = isset($cell['_a']['ss:Index']) ? $cell['_a']['ss:Index'] : $colidx+1;
      $data[$rowidx][$colidx] = _extract_xml_cell($cell);
    }
    // dpm($rowidx);
    // dpm($data[$rowidx]);
  }
  return $data;
}

function _extract_xml_cell($cell) {
  if ( !is_array($cell) ) return 'ERROR: CELL NOT AN ARRAY';
  $value = $cell['_c']['Data']['_v'];
  $value = preg_replace(
    array( '/<Font[^>]*\>/', '/<\/Font\>/' ),
    '',
    $value
  );
  return $value;
}

function _reference_lister(&$refarray, $reference, $refid = NULL) {
  if ( isset($refarray[$reference]) ) {
    if ( isset($refid) && ($refid != $refarray[$reference]) ) {
      // dpm("conflicting refid: $refid for $reference");
    }
    return $refarray[$reference];
  }
  if ( !isset($refid) ) {
    if (!isset($refarray['_lastid']) ) {
      $refarray['_lastid'] = $refid = 1;
    }
    else {
      $refid = ++$refarray['_lastid'];
    }
  }
  $refarray[$reference] = $refid;
  return $refid;
}

function _build_side_table($data, $table_name, $key_name, $value_names, $descriptor) {
  $value_name_string = implode(',', $value_names);
  $sql = "INSERT INTO {$table_name} ($key_name,$value_name_string) VALUES ($descriptor)";
  foreach ($data as $key => $value) {
    if ($key=='_lastid') continue;
    if ( is_array($value) ) {
      $valarray = array_merge( array($key), $value );
    }
    else {
      $valarray = array( $key, $value );
    }
    db_query( $sql, $valarray );
  }
}

function _build_relational_table($data, $table_name, $value1, $value2, $descriptor) {
  $sql = "INSERT INTO {$table_name} ($value1,$value2) VALUES ($descriptor)";
  foreach ($data as $row) {
    db_query( $sql, $row );
  }
}


// following xml-to-array functions published at:
//
//		http://mysrc.blogspot.com/2007/02/php-xml-to-array-and-backwards.html
//
// XML to Array
function xml2ary(&$string) {
  $parser = xml_parser_create();
  xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
  xml_parse_into_struct($parser, $string, $vals, $index);
  // dpm($vals);
  xml_parser_free($parser);

  $mnary=array();
  $ary=&$mnary;
  foreach ($vals as $r) {
    $t=$r['tag'];
    if ($r['type']=='open') {
      if (isset($ary[$t])) {
        if (isset($ary[$t][0])) $ary[$t][]=array();
        else $ary[$t]=array($ary[$t], array());
        $cv=&$ary[$t][count($ary[$t])-1];
      }
      else $cv=&$ary[$t];
      if (isset($r['attributes'])) {
        foreach ($r['attributes'] as $k => $v) $cv['_a'][$k]=$v;
      }
      $cv['_c']=array();
      $cv['_c']['_p']=&$ary;
      $ary=&$cv['_c'];

    }
    elseif ($r['type']=='complete') {
      if (isset($ary[$t])) { // same as open
        if (isset($ary[$t][0])) $ary[$t][]=array();
        else $ary[$t]=array($ary[$t], array());
        $cv=&$ary[$t][count($ary[$t])-1];
      }
      else $cv=&$ary[$t];
      if (isset($r['attributes'])) {
        foreach ($r['attributes'] as $k => $v) $cv['_a'][$k]=$v;
      }
      $cv['_v']=(isset($r['value']) ? $r['value'] : '');

    }
    elseif ($r['type']=='close') {
      $ary=&$ary['_p'];
    }
  }    
  
  _del_p($mnary);
  return $mnary;
}

// _Internal: Remove recursion in result array
function _del_p(&$ary) {
  foreach ($ary as $k => $v) {
    if ($k==='_p') unset($ary[$k]);
    elseif (is_array($ary[$k])) _del_p($ary[$k]);
  }
}

// Array to XML
function ary2xml($cary, $d=0, $forcetag='') {
  $res=array();
  foreach ($cary as $tag => $r) {
    if (isset($r[0])) {
      $res[]=ary2xml($r, $d, $tag);
    }
    else {
      if ($forcetag) $tag=$forcetag;
      $sp=str_repeat("\t", $d);
      $res[]="$sp<$tag";
      if (isset($r['_a'])) {
        foreach ($r['_a'] as $at => $av) $res[]=" $at=\"$av\"";
      }
      $res[]=">" . ((isset($r['_c'])) ? "\n" : '');
      if (isset($r['_c'])) $res[]=ary2xml($r['_c'], $d+1);
      elseif (isset($r['_v'])) $res[]=$r['_v'];
      $res[]=(isset($r['_c']) ? $sp : '') . "</$tag>\n";
    }
        
  }
  return implode('', $res);
}

// Insert element into array
function ins2ary(&$ary, $element, $pos) {
  $ar1=array_slice($ary, 0, $pos); $ar1[]=$element;
  $ary=array_merge($ar1, array_slice($ary, $pos));
}
